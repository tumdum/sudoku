package main

import (
	"bufio"
	"bytes"
	"testing"
)

const solvedInput = `192456378
734928156
658731924
247695831
386147592
915283467
421369785
569874213
873512649`

const solvedInputFlat = `192456378734928156658731924247695831386147592915283467421369785569874213873512649`

func MustParseBoard(input string) *Board {
	b, err := ParseBoard(bufio.NewReader(bytes.NewReader([]byte(input))))
	if err != nil {
		panic(err)
	}
	return b
}

func TestNewBoard(t *testing.T) {
	b := NewBoard()
	var expectedField Field
	fillField(&expectedField)
	for i := 1; i <= 9; i++ {
		for j := 1; j <= 9; j++ {
			p := Position{i, j}
			if !b.Get(p).Equal(expectedField) {
				t.Fatalf("Expected at %v '%v', got '%v'", p, b.Get(p), expectedField)
			}
		}
	}
}

func AllPossibleExcept(b Board, p Position, value int) bool {
	for i := 1; i <= 9; i++ {
		if i == value {
			if b.IsPossible(p, value) {
				return false
			}
		} else if !b.IsPossible(p, i) {
			return false
		}
	}
	return true
}

func CheckAllPossibleExcept(b Board, p Position, value int, t *testing.T) {
	if !AllPossibleExcept(b, p, value) {
		t.Fatalf("'%v' should not be possible at '%v', but is '%v'", value, p, b.Get(p))
	}
}

func TestBoxRange(t *testing.T) {
	data := []struct {
		input     int
		low, high int
	}{
		{1, 1, 3}, {2, 1, 3}, {3, 1, 3},
		{4, 4, 6}, {5, 4, 6}, {6, 4, 6},
		{7, 7, 9}, {8, 7, 9}, {9, 7, 9},
	}
	for _, d := range data {
		low, high := boxRangeFromPos(d.input)
		if low != d.low || high != d.high {
			t.Fatalf("For %d expected %d,%d got %d,%d", d.input, d.low, d.high, low, high)
		}
	}
}

func TestRemovingValueShouldPropagate(t *testing.T) {
	b := NewBoard()
	val := 9
	pos := Position{1, 1}
	b.Set(pos, val)
	for i := 2; i <= 9; i++ {
		CheckAllPossibleExcept(b, Position{i, 1}, val, t)
		CheckAllPossibleExcept(b, Position{1, i}, val, t)
	}

	boxpos := []Position{
		{1, 2}, {1, 3}, {2, 1}, {2, 2}, {2, 3},
		{3, 1}, {3, 2}, {3, 3},
	}

	for _, p := range boxpos {
		CheckAllPossibleExcept(b, p, val, t)
	}

	if !b.IsPossible(pos, val) {
		t.Fatalf("%v should be possible at '%v'", val, pos)
	}
}

func TestParseBoard(t *testing.T) {
	parsed := MustParseBoard(solvedInput)
	flat := MustParseBoard(solvedInputFlat)

	b := NewBoard()
	setRow := func(rowId int, row [9]int) {
		for col, val := range row {
			b.Set(Position{rowId, col + 1}, val)
		}
	}
	setRow(1, [9]int{1, 9, 2, 4, 5, 6, 3, 7, 8})
	setRow(2, [9]int{7, 3, 4, 9, 2, 8, 1, 5, 6})
	setRow(3, [9]int{6, 5, 8, 7, 3, 1, 9, 2, 4})
	setRow(4, [9]int{2, 4, 7, 6, 9, 5, 8, 3, 1})
	setRow(5, [9]int{3, 8, 6, 1, 4, 7, 5, 9, 2})
	setRow(6, [9]int{9, 1, 5, 2, 8, 3, 4, 6, 7})
	setRow(7, [9]int{4, 2, 1, 3, 6, 9, 7, 8, 5})
	setRow(8, [9]int{5, 6, 9, 8, 7, 4, 2, 1, 3})
	setRow(9, [9]int{8, 7, 3, 5, 1, 2, 6, 4, 9})

	if !parsed.Equal(b) {
		t.Fatal()
	}
	if !flat.Equal(b) {
		t.Fatal()
	}
}

func TestSolvedDetection(t *testing.T) {
	b := MustParseBoard(solvedInput)
	if !b.IsSolved() {
		t.Fatalf("Expected solved board to be solved")
	}
}

func TestCloneBoard(t *testing.T) {
	b := MustParseBoard(solvedInput)
	clone := b.Clone()
	if !clone.Equal(*b) {
		t.Fatal()
	}
}

func TestNoSimplification(t *testing.T) {
	b := MustParseBoard(solvedInput)
	simplified := NewSimplifier().Simplify(b)
	if !simplified.IsSolved() {
		t.Fatal()
	}
}

func TestOneAmbiguity(t *testing.T) {
	inputCopy := "_" + solvedInput[1:]
	b := MustParseBoard(inputCopy)
	if !b.IsSolved() {
		t.Fatal()
	}
}

func TestDuplicatesChecker(t *testing.T) {
	hasDuplicates, checker := makeDuplicatesChecker()
	checker(Position{1, 1}, NewSingletonField(1))
	checker(Position{1, 2}, NewSingletonField(2))
	if *hasDuplicates {
		t.Fatal()
	}
	checker(Position{1, 3}, NewSingletonField(1))
	if !*hasDuplicates {
		t.Fatal()
	}
}

func TestAmbigouseBoard(t *testing.T) {
	input := `98765____
24___3_85
__1_2____
___5_7___
__4___1__
_9_______
5______73
__2_1____
____4___9`
	b := MustParseBoard(input)
	if b.IsSolved() {
		t.Fatal()
	}
	s := NewSimplifier()
	simplified := s.Simplify(b)
	expected := `987654321
246173985
351928746
128537694
634892157
795461832
519286473
472319568
863745219`
	expectedBoard := MustParseBoard(expected)
	if !expectedBoard.Equal(*simplified) {
		t.Fatal()
	}
}
