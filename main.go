package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"time"
	"unicode"
)

var boxes = []Position{
	{1, 1}, {4, 1}, {7, 1},
	{1, 4}, {4, 4}, {7, 4},
	{1, 7}, {4, 7}, {7, 7},
}

type Position struct {
	Row, Col int
}

type Field struct {
	possible [10]bool
	size     int
}

func (f Field) String() string {
	elements := []int{}
	for i, v := range f.possible {
		if v {
			elements = append(elements, i)
		}
	}
	return fmt.Sprintf("%v", elements)
}

func (f Field) Clone() *Field {
	return &Field{f.possible, f.size}
}

func (f Field) Size() int {
	return f.size
}

func (f Field) IsPossible(value int) bool {
	return f.possible[value]
}

func (f Field) IsEmpty() bool {
	return f.Size() == 0
}

func (f Field) IsSingleton() bool {
	return f.Size() == 1
}

func (f Field) Any() int {
	for k, v := range f.possible {
		if v {
			return k
		}
	}
	panic("empty field asked for value")
}

func (f *Field) Remove(value int) {
	if f.possible[value] {
		f.possible[value] = false
		f.size--
	}
}

func (f *Field) Add(value int) {
	if !f.possible[value] {
		f.possible[value] = true
		f.size++
	}
}

func (f Field) Equal(other Field) bool {
	for k, v := range f.possible {
		if other.possible[k] != v {
			return false
		}
	}
	return true
}

func fillField(f *Field) {
	f.possible[1] = true
	f.possible[2] = true
	f.possible[3] = true
	f.possible[4] = true
	f.possible[5] = true
	f.possible[6] = true
	f.possible[7] = true
	f.possible[8] = true
	f.possible[9] = true
	f.size = 9
}

func NewSingletonField(value int) *Field {
	f := Field{}
	f.Add(value)
	return &f
}

type BoardVisitor func(Position, *Field)

type Board struct {
	fields [10][10]Field
}

func (b Board) String() string {
	var buf bytes.Buffer
	b.ForAll(func(p Position, f *Field) {
		fmt.Fprintf(&buf, "%v", f.String())
		if p.Col == 9 {
			fmt.Fprintln(&buf)
		}
	})
	return buf.String()
}

func (b *Board) Clone() Board {
	clone := NewBoard()
	for row := 1; row <= 9; row++ {
		for col := 1; col <= 9; col++ {
			p := Position{row, col}
			clone.OverrideField(p, b.Get(p).Clone())
		}
	}
	return clone
}

func (b *Board) Equal(o Board) bool {
	for row := 1; row <= 9; row++ {
		for col := 1; col <= 9; col++ {
			p := Position{row, col}
			if !b.Get(p).Equal(*o.Get(p)) {
				return false
			}
		}
	}
	return true
}

func (b *Board) Get(p Position) *Field {
	return &b.fields[p.Row][p.Col]
}

func (b *Board) Set(p Position, value int) {
	remove := func(p Position, f *Field) { f.Remove(value) }
	b.ForRow(p, remove)
	b.ForCol(p, remove)
	b.ForBox(p, remove)
	b.OverrideField(p, NewSingletonField(value))
}

func (b *Board) OverrideField(p Position, f *Field) {
	b.fields[p.Row][p.Col] = *f
}

func (b *Board) IsPossible(p Position, value int) bool {
	return b.Get(p).IsPossible(value)
}

func (b *Board) ForRow(inRow Position, visitor BoardVisitor) {
	for col := 1; col <= 9; col++ {
		p := Position{inRow.Row, col}
		visitor(p, b.Get(p))
	}
}

func (b *Board) ForCol(inCol Position, visitor BoardVisitor) {
	for row := 1; row <= 9; row++ {
		p := Position{row, inCol.Col}
		visitor(p, b.Get(p))
	}
}

func (b *Board) ForAll(visitor BoardVisitor) {
	for row := 1; row <= 9; row++ {
		b.ForRow(Position{row, 1}, visitor)
	}
}

func (b *Board) IsSolved() bool {
	return b.AllRowsAreSolved() &&
		b.AllColsAreSolved() &&
		b.AllBoxesAreSolved()
}

func makeConstraintChecker(ret *bool) BoardVisitor {
	foundCounterExample := false
	seen := Field{}
	return func(p Position, f *Field) {
		if foundCounterExample {
			return
		}
		if !f.IsSingleton() {
			*ret = false
			foundCounterExample = true
		} else if single := f.Any(); seen.IsPossible(single) {
			*ret = false
			foundCounterExample = true
		} else {
			seen.Add(single)
		}
	}
}

func (b *Board) AllRowsAreSolved() bool {
	allRowsAreSolved := true
	for row := 1; row <= 9; row++ {
		checker := makeConstraintChecker(&allRowsAreSolved)
		b.ForRow(Position{row, 1}, checker)
	}
	return allRowsAreSolved
}

func (b *Board) AllColsAreSolved() bool {
	allColsAreSolved := true
	for col := 1; col <= 9; col++ {
		checker := makeConstraintChecker(&allColsAreSolved)
		b.ForCol(Position{1, col}, checker)
	}
	return allColsAreSolved
}

func (b *Board) AllBoxesAreSolved() bool {
	allBoxesAreSolved := true
	for _, box := range boxes {
		checker := makeConstraintChecker(&allBoxesAreSolved)
		b.ForBox(box, checker)
	}
	return allBoxesAreSolved
}

func (b *Board) ForBox(inBox Position, visitor BoardVisitor) {
	rowLow, rowHigh := boxRangeFromPos(inBox.Row)
	colLow, colHigh := boxRangeFromPos(inBox.Col)
	for row := rowLow; row <= rowHigh; row++ {
		for col := colLow; col <= colHigh; col++ {
			p := Position{row, col}
			visitor(p, b.Get(p))
		}
	}
}

func (b *Board) Show(w io.Writer) {
	b.ForAll(func(p Position, f *Field) {
		if f.IsSingleton() {
			fmt.Fprintf(w, "%d ", f.Any())
		} else if f.IsEmpty() {
			fmt.Fprintf(w, "X ")
		} else {
			fmt.Fprintf(w, ". ")
		}
		if p.Col == 3 || p.Col == 6 {
			fmt.Fprintf(w, "|")
		} else if p.Col == 9 {
			fmt.Fprintln(w)
			if p.Row == 3 || p.Row == 6 {
				fmt.Fprintln(w, "------+------+------")
			}
		}
	})
}

func boxRangeFromPos(pos int) (low, high int) {
	p := pos - 1
	return (p/3)*3 + 1, ((p / 3) + 1) * 3
}

func NewBoard() Board {
	b := Board{}
	for row := 1; row <= 9; row++ {
		for col := 1; col <= 9; col++ {
			fillField(&b.fields[row][col])
		}
	}
	return b
}

func readPrintableChars(r *bufio.Reader, n int) ([]byte, error) {
	out := make([]byte, n)
	c := 0
	for c < n {
		b, err := r.ReadByte()
		if err != nil {
			return nil, err
		}
		if unicode.IsPrint(rune(b)) {
			out[c] = b
			c++
		}
	}
	return out, nil
}

func ParseBoard(r *bufio.Reader) (*Board, error) {
	b := NewBoard()
	input, err := readPrintableChars(r, 81)
	if err != nil {
		return nil, err
	}
	reader := bytes.NewReader(input)
	for row := 1; row <= 9; row++ {
		t := [9]int{}
		_, err := fmt.Fscanf(reader, "%c%c%c%c%c%c%c%c%c", &t[0], &t[1], &t[2], &t[3], &t[4], &t[5], &t[6], &t[7], &t[8])
		if err != nil {
			return nil, err
		}
		for i, v := range t {
			if v != '_' && v != '.' {
				b.Set(Position{row, i + 1}, v-'0')
			}
		}
	}
	return &b, nil
}

type Simplifier struct {
}

func NewSimplifier() Simplifier {
	return Simplifier{}
}

func (s Simplifier) Simplify(b *Board) *Board {
	if s.isIncorrect(b) {
		return nil
	} else if b.IsSolved() {
		return b
	}

	smallestPos := s.findSmallestAmbigouity(b)

	for k, v := range b.Get(smallestPos).possible {
		if v {
			possible := b.Clone()
			possible.Set(smallestPos, k)
			if simple := s.Simplify(&possible); simple != nil {
				return simple
			}
		}
	}

	return nil
}

func (s Simplifier) isIncorrect(b *Board) bool {
	incorrect := false
	b.ForAll(func(p Position, f *Field) {
		if f.IsEmpty() {
			incorrect = true
		}
	})
	if incorrect {
		return true
	}
	for row := 1; row <= 9; row++ {
		hasDuplicates, checker := makeDuplicatesChecker()
		b.ForRow(Position{row, 1}, checker)
		if *hasDuplicates {
			return true
		}
	}

	for col := 1; col <= 9; col++ {
		hasDuplicates, checker := makeDuplicatesChecker()
		b.ForCol(Position{1, col}, checker)
		if *hasDuplicates {
			return true
		}
	}

	for _, box := range boxes {
		hasDuplicates, checker := makeDuplicatesChecker()
		b.ForBox(box, checker)
		if *hasDuplicates {
			return true
		}
	}
	return false
}

func (s Simplifier) findSmallestAmbigouity(b *Board) Position {
	var smallestPos *Position

	b.ForAll(func(p Position, f *Field) {
		if smallestPos == nil && f.Size() > 1 {
			smallestPos = &p
		} else if smallestPos == nil {
			return
		}
		if f.Size() > 1 && f.Size() < b.Get(*smallestPos).Size() {
			smallestPos = &p
		}
	})

	return *smallestPos
}

func makeDuplicatesChecker() (*bool, BoardVisitor) {
	seen := Field{}
	var hasDuplicates bool
	return &hasDuplicates, func(p Position, f *Field) {
		if !f.IsSingleton() {
			return
		}
		if single := f.Any(); seen.IsPossible(single) {
			hasDuplicates = true
		} else {
			seen.Add(single)
		}
	}
}

func run(r io.Reader, w io.Writer) {
	br := bufio.NewReader(r)
	durations := []time.Duration{}
	puzzles := int64(0)
	for {
		board, err := ParseBoard(br)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Fprintf(os.Stderr, "Failed to parse input board: %v", err)
				os.Exit(1)
			}
		}
		start := time.Now()
		solution := NewSimplifier().Simplify(board)
		duration := time.Since(start)
		if solution == nil {
			fmt.Fprintf(os.Stderr, "Failed to find solution for input board.")
		}
		board.Show(w)
		fmt.Println()
		solution.Show(w)
		fmt.Fprintf(w, "\n(%.2f seconds)\n\n", duration.Seconds())

		durations = append(durations, duration)
		puzzles++
	}
	total := time.Duration(0)
	for _, d := range durations {
		total += d
	}
	if puzzles > 0 {
		avg := total / time.Duration(puzzles)
		fmt.Printf("avg: %.3fs\n", avg.Seconds())
	}
}

func main() {
	input := os.Stdin
	output := os.Stdout
	run(input, output)
}
